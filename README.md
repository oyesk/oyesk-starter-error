# Oyesk Starter Error
This is a custom library made by OYESK team that adds some basic exceptions and an exception handler to handle all the thrown exceptions through the Maven build tool.

## Features
 * Custom common exceptions like: `BadRequestException`, `NotFoundException` and `DetailedException` (consult the implementation on the source code);
 * Custom ExceptionHandler that handles all thrown exceptions.

## Version
Current version: **2.6.6**
> **_NOTE:_** The version of this library is always the same as its parent [spring-boot-starter-parent](https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-parent) version

## Usage
 1. Add this into your `pom.xml` to start using `oyesk-starter-error` dependency:

```xml
<!-- add oyesk-starter-error to dependencies -->
<dependencies>
    <dependency>
        <groupId>com.oyesk.starter</groupId>
        <artifactId>oyesk-starter-error</artifactId>
        <version>2.6.6</version>
    </dependency>
</dependencies>
```

 2. You can enable the features of this library by adding this line into your `application.properties`:

```properties
oyesk.exception-handler.enabled=true
```

 3. You can create your own exceptions with different response status and use them

## Custom Configuration or Override
This library can be customized to the needs of each project, if you need to change the `HttpExceptionHandler` that adds the http exception handler provided by this library, you can create a `@Bean` with the exact same name that returns your own CustomHttpExceptionHandler. That will override the the exception handler provided by this library.
This is the `Bean` provided by this library (the implementation can be consulted on the library source code): 

```java
@Bean
public HttpExceptionHandler httpExceptionHandler() { return new HttpExceptionHandler(); }
```

An example of a custom exception handler that just adds on to the exception handler provided by this library, would be:

```java
@ControllerAdvice
public class MyCustomHttpExceptionHandler extends HttpExceptionHandler {

    @ExceptionHandler(MyCustomException.class)
    protected ResponseEntity<Object> handleMyCustomStatusException(MyCustomException ex, WebRequest request) {

        var errorBody = new ErrorBody();
        errorBody.setTimestamp(ex.getErrorBody().getTimestamp());
        errorBody.setMessage(ex.getMessage());
        errorBody.setStatus(ex.getErrorBody().getStatus());
        errorBody.setError(ex.getErrorBody().getError());
        errorBody.setDetails(ex.getErrorBody().getDetails());
        errorBody.setPath(ex.getErrorBody().getPath());

        return handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(ex.status()), request);
    }
}
```

If you want to add some other configuration related to the exception handler, you can create a `@Bean` with your configurations on a `@Configuration` class.

[//]: # (## Public Domain)

[//]: # (...)

[//]: # ()
[//]: # (## License)

[//]: # (...)

[//]: # ()
[//]: # ()
[//]: # (## Privacy)

[//]: # (...)

[//]: # ()
[//]: # (## Contributing)

[//]: # (...)

[//]: # ()
[//]: # (## Records)

[//]: # (...)

[//]: # ()
[//]: # (## Notices)

[//]: # (...)
