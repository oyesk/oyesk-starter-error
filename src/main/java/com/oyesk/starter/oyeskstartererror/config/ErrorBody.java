package com.oyesk.starter.oyeskstartererror.config;

import com.oyesk.starter.oyeskstartererror.exception.ExceptionDetail;
import org.springframework.http.HttpStatus;

import java.time.Instant;
import java.util.List;

public class ErrorBody {
    private Instant timestamp;
    private int status;
    private String error;
    private String message;
    private String path;
    private List<ExceptionDetail> details;

    public ErrorBody() {
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status.value();
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<ExceptionDetail> getDetails() {
        return details;
    }

    public void setDetails(List<ExceptionDetail> details) {
        this.details = details;
    }

}
