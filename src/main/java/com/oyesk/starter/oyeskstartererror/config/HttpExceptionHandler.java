package com.oyesk.starter.oyeskstartererror.config;


import com.oyesk.starter.oyeskstartererror.exception.DetailedException;
import com.oyesk.starter.oyeskstartererror.exception.ExceptionDetail;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@ControllerAdvice
public class HttpExceptionHandler extends ResponseEntityExceptionHandler {

    private final Map<Class, HttpStatus> cache = new ConcurrentHashMap<>();

    @ExceptionHandler(value = {DetailedException.class})
    protected ResponseEntity<Object> handleDetailedException(DetailedException e, WebRequest request) {

        var body = buildBody(e, e.getExceptionDetails(), request);

        return handleExceptionInternal(e, body, new HttpHeaders(), HttpStatus.valueOf(body.getStatus()), request);
    }

    @ExceptionHandler(value = {RuntimeException.class})
    protected ResponseEntity<Object> handleRuntimeException(Exception e, WebRequest request) {

        var body = buildBody(e, List.of(), request);

        return handleExceptionInternal(e, body, new HttpHeaders(), HttpStatus.valueOf(body.getStatus()), request);
    }

    private ErrorBody buildBody(Exception e, List<ExceptionDetail> details, WebRequest request) {
        return buildBody(e, details, request, getStatus(e));
    }

    private ErrorBody buildBody(Exception e, List<ExceptionDetail> details, WebRequest request, HttpStatus status) {
        var message = "";
        if (null != e.getCause() && null != e.getCause().getMessage()) {
            message = e.getCause().getMessage();
        } else if (null != e.getMessage()) {
            message = e.getMessage();
        }

        var errorBody = new ErrorBody();
        errorBody.setTimestamp(Instant.now());
        errorBody.setError(e.toString());
        errorBody.setMessage(message);
        errorBody.setStatus(status);
        errorBody.setDetails(details);
        errorBody.setPath(((ServletWebRequest) request).getRequest().getRequestURI());

        return errorBody;
    }

    private HttpStatus getStatus(Throwable e) {
        var status = findByResponseStatusAnnotation(e);

        if (status == null) {
            status = findByResponseStatusAnnotation(e.getCause());
        }

        return status == null ? INTERNAL_SERVER_ERROR : status;
    }

    private @Nullable HttpStatus findByResponseStatusAnnotation(Throwable e) {
        if (e == null) {
            return null;
        }

        var clazz = e.getClass();

        if (cache.containsKey(clazz)) {
            return cache.get(clazz);
        }

        var annotation = AnnotationUtils.findAnnotation(clazz, ResponseStatus.class);

        if (annotation != null) {
            cache.put(clazz, annotation.value());

            return annotation.value();
        }

        return null;
    }
}
