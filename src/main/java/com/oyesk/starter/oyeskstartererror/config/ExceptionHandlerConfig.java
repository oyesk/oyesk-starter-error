package com.oyesk.starter.oyeskstartererror.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(name = "oyesk.exception-handler.enabled", havingValue = "true")
public class ExceptionHandlerConfig {

    @Bean
    @ConditionalOnMissingBean
    public HttpExceptionHandler httpExceptionHandler() {
        return new HttpExceptionHandler();
    }
}
