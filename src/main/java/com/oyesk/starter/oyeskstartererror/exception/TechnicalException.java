package com.oyesk.starter.oyeskstartererror.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serial;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class TechnicalException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = -4107043109561249434L;

    public TechnicalException() {
        super();
    }

    public TechnicalException(String message) {
        super(message);
    }

    public TechnicalException(String message, Throwable cause) {
        super(message, cause);
    }

}
