package com.oyesk.starter.oyeskstartererror.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serial;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class SearchSyncException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = -8687944189883339647L;

    public SearchSyncException() {
        super();
    }

    public SearchSyncException(String message) {
        super(message);
    }

    public SearchSyncException(String message, Throwable cause) {
        super(message, cause);
    }
}
