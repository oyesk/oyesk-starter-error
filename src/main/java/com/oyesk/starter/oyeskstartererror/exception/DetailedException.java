package com.oyesk.starter.oyeskstartererror.exception;


import java.io.Serial;
import java.util.List;

public class DetailedException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 6833656491181774986L;

    private final List<ExceptionDetail> exceptionDetails;

    public DetailedException(List<ExceptionDetail> exceptionDetails) {
        super();
        this.exceptionDetails = exceptionDetails;
    }

    public DetailedException(List<ExceptionDetail> exceptionDetails, String message) {
        super(message);
        this.exceptionDetails = exceptionDetails;
    }

    public DetailedException(List<ExceptionDetail> exceptionDetails, String message, Throwable cause) {
        super(message, cause);
        this.exceptionDetails = exceptionDetails;
    }

    public List<ExceptionDetail> getExceptionDetails() {
        return exceptionDetails;
    }
}
