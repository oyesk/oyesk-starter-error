package com.oyesk.starter.oyeskstartererror.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serial;
import java.util.List;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends DetailedException {

    @Serial
    private static final long serialVersionUID = -2328590512386854747L;

    public BadRequestException(List<ExceptionDetail> exceptionDetail) {
        super(exceptionDetail);
    }

    public BadRequestException(List<ExceptionDetail> exceptionDetail, String message) {
        super(exceptionDetail, message);
    }

    public BadRequestException(List<ExceptionDetail> exceptionDetail, String message, Throwable cause) {
        super(exceptionDetail, message, cause);
    }

}
