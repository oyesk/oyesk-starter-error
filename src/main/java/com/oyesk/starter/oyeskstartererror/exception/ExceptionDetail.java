package com.oyesk.starter.oyeskstartererror.exception;


public class ExceptionDetail {

    private String reason;
    private String message;

    public ExceptionDetail() {
    }

    public ExceptionDetail(String reason, String message) {
        this.reason = reason;
        this.message = message;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
